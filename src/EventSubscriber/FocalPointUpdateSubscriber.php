<?php
/**
 * Focal Point S3fs Cache - Invalidation logic for s3 image cache on focal
 * point update
 *
 * @package     focal_point_s3fs_cache
 * @author      Udit Rawat <uditrawat@fabwebstudio.com>
 * @license     GPL-2.0+
 * @link        http://www.fabwebstudio.com/
 * @copyright   FabWebStudio
 * Date:        04/16/2019
 * Time:        12:38 PM
 */

namespace Drupal\focal_point_s3fs_cache\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\focal_point_s3fs_cache\Event\FocalPointUpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Aws\CloudFront\CloudFrontClient;
use Drupal\image\Entity\ImageStyle;


class FocalPointUpdateSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The admin route context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Config factory object
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger object
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;


  /**
   * Cloud Front Client
   *
   * @var null
   */
  protected $client = NULL;

  /**
   * The Drupal kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $entityTypeManager;

  /**
   * FocalPointUpdateSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   */
  public function __construct(
    AccountProxyInterface $account,
    AdminContext $adminContext,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $logger
  ) {
    $this->account = $account;
    $this->adminContext = $adminContext;
    $this->configFactory = $configFactory;
    $this->logger = $logger->get('focal_point_s3fs_cache');
  }

  /**
   * On Event Update
   *
   * @param \Drupal\focal_point_s3fs_cache\Event\FocalPointUpdateEvent $event
   */
  public function onUpdate(FocalPointUpdateEvent $event) {

    $uri = $event->getUri();
    if ($uri) {

      // s3ici settings
      $config = $this->configFactory->get('focal_point_s3fs_cache.settings');
      $distribution_id = $config->get('s3_distribution_id');
      $image_style = $config->get('selected_styles');

      // S3fs keys
      $s3fs_config = $this->configFactory->get('s3fs.settings');
      $access_key = $s3fs_config->get('access_key');
      $secret_key = $s3fs_config->get('secret_key');
      $region = $s3fs_config->get('region') ? $s3fs_config->get('region') : 'eu-west-2';

      try {
        // Create a CloudFront Client
        $this->client = new CloudFrontClient([
          'version' => 'latest',
          'region' => $region,
          'credentials' => [
            'key' => $access_key,
            'secret' => $secret_key,
          ],
        ]);
      } catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }


      if (!empty($image_style) && !empty($distribution_id)) {
        $url = [];
        foreach ($image_style as $key => $value) {
          // remove slides which are not selected
          // if they are still coming in
          if($value === '0'){
            continue;
          }

          // Generate image style url
          $temp_url = ImageStyle::load($key)
            ->buildUrl($uri);
          $temp_url = strtok($temp_url, '?');
          $url_arr = explode('/styles/', $temp_url);
          $url[] = '/styles/' . $url_arr[1];
        }
        if (!empty($url)) {
          $this->flushImageCache($url, $distribution_id);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FocalPointUpdateEvent::FOCAL_POINT_UPDATE => ['onUpdate', -256],
    ];
  }

  /**
   * Flush aws image cache
   *
   * @param $url
   * @param $distribution_id
   *
   * @return bool
   */
  private function flushImageCache($url, $distribution_id) {

    if (empty($url) || empty($distribution_id)) {
      return FALSE;
    }

    if ($this->client) {
      try {
        $result = $this->client->createInvalidation([
          'DistributionId' => $distribution_id,
          'InvalidationBatch' => [
            'CallerReference' => date('c'),
            'Paths' => [
              'Items' => $url,
              'Quantity' => count($url),
            ],
          ],
        ]);
        $this->logger->notice(serialize($result));
      } catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }
  }

}
