<?php
/**
 * Focal Point S3fs Cache - Invalidation logic for s3 image cache on focal
 * point update
 *
 * @package     focal_point_s3fs_cache
 * @author      Udit Rawat <uditrawat@fabwebstudio.com>
 * @license     GPL-2.0+
 * @link        http://www.fabwebstudio.com/
 * @copyright   FabWebStudio
 * Date:        04/16/2019
 * Time:        12:38 PM
 */
namespace Drupal\focal_point_s3fs_cache\Event;

use Symfony\Component\EventDispatcher\Event;

class FocalPointUpdateEvent  extends Event{

  const FOCAL_POINT_UPDATE = 'focal_point.update';

  /**
   * Image uri
   *
   * @var string $uri
   */
  protected $uri;

  /**
   * FocalPointUpdateEvent constructor.
   *
   * @param string $uri
   */
  public function __construct($uri = '') {
    $this->uri = $uri;
  }

  /**
   * Get inserted uri
   *
   * @return string
   */
  public function getUri() {
    return $this->uri;
  }

}