<?php
/**
 * Focal Point S3fs Cache - Invalidation logic for s3 image cache on focal
 * point update
 *
 * @package     focal_point_s3fs_cache
 * @author      Udit Rawat <uditrawat@fabwebstudio.com>
 * @license     GPL-2.0+
 * @link        http://www.fabwebstudio.com/
 * @copyright   FabWebStudio
 * Date:        04/16/2019
 * Time:        12:38 PM
 */

namespace Drupal\focal_point_s3fs_cache\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class S3ImageCacheSettingForm
 *
 * @package Drupal\s3imagecache\Form
 */
class SettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fp_s3fs_c_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'focal_point_s3fs_cache.settings',
    ];
  }

  /**
   * Entity type manager object
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * SettingForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(ConfigFactoryInterface $configFactory,
                              EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configFactory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('focal_point_s3fs_cache.settings');
    $form['s3_distribution_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Distribution ID'),
      '#default_value' => $config->get('s3_distribution_id'),
      '#description' => $this->t("AWS Distribution id."),
      '#required' => TRUE,
    ];

    // load all styles
    $styles = $this->entityTypeManager
      ->getStorage('image_style')
      ->loadMultiple();

    if (!empty($styles)) {
      foreach ($styles as $key => $style) {
        $options[$style->id()] = $style->label();
      }

      $form['selected_styles'] = [
        '#type' => 'checkboxes',
        '#title' => t('Choose Style'),
        '#options' => $options,
        '#description' => t('choose style to be update dynamically on AWS'),
        '#default_value' => $config->get('selected_styles')
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // filtering selected styles
    if(is_array($values['selected_styles']) && !empty($values['selected_styles'])){
      foreach ($values['selected_styles'] as $key => $val){
        if($val === 0){
          unset($values['selected_styles'][$key]);
        }
      }
    }

    // saving the configuration
    $this->config('focal_point_s3fs_cache.settings')
      ->set('s3_distribution_id', $values['s3_distribution_id'])
      ->set('selected_styles', $values['selected_styles'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}